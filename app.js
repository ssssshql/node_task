const schedule = require('node-schedule');
const fs=require('fs')
const shell = require('shelljs');

const scriptPath='./script/'

function exec(){
    fs.readdir(scriptPath, (err, files)=>{
        files.forEach(path=>{
            if(/(.py)$/.test(path)){ // py脚本
                shell.exec(`python ${scriptPath+path}`)
            }else if(/(.js)$/.test(path)){ // js脚本
                shell.exec(`node ${scriptPath+path}`)
            }
            // 还有其他脚本也可以加，但要环境支持
        })
    })
}

// 启动服务时立马执行一次
//exec()

// 0 0 12 * * ? cron表达式，每天12点执行脚本
schedule.scheduleJob('0 0 12 * * ?', () => {
    exec()
});