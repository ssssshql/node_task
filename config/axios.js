const axios = require("axios")
const instance = axios.create({
    // `withCredentials` 表示跨域请求时是否需要使用凭证
    withCredentials: true, // default
})

// Add a response interceptor
instance.interceptors.response.use(function (response) {
    // Do something with response data
    return response.data;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });

module.exports=instance